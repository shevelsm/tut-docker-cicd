FROM python:3.7.2-alpine

RUN adduser -D django
USER django
WORKDIR /home/django
ENV PYTHONUNBUFFERED=1
ENV PATH="/home/django_user/.local/bin:${PATH}"

COPY --chown=django:django requirements.txt requirements.txt
RUN pip install --user --no-cache-dir -r requirements.txt

COPY --chown=django:django . .

RUN mkdir db && \
    touch db/db.sqlite3 && \
    python manage.py migrate

CMD python3 manage.py runserver 0.0.0.0:8000
